library(ggplot2)
library(scales)

expotime = read.csv("expotime.csv", sep=";", colClasses=c("numeric", "POSIXct"), col.names=c("expotime", "datetime"))

expotime$time <- as.POSIXct(strftime(expotime$datetime, format="%H:%M:%S"), format="%H:%M:%S")
ggplot(expotime,
       aes(time, 1/expotime)) +
  geom_point(alpha=0.1, size=1) +
  scale_y_log10(breaks=c(1,2,5,10,20,50,100,200,500,1000,2000)) +
  scale_x_datetime(breaks = date_breaks("1 hour"),
                     labels = date_format("%H")) +
  stat_smooth(colour="black", alpha=0.6)
ggsave("expotime2.png")

# ten most frequent exposure time values
sort(table(1/expotime$expotime), decreasing = TRUE)[1:10]
