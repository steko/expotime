import datetime
import os
import sqlite3
from os.path import join

from gi.repository import GExiv2, GLib

exif = GExiv2.Metadata()

def expowalk(dirname):
    for directory, dirnames, filenames in os.walk(dirname):
        for f in filenames:
            try:
                exif.open_path(join(directory, f))
            except GLib.Error:
                pass
            else:
                expotime_n, expotime_d = exif.get_exposure_time()
                try:
                    expotime = expotime_n / expotime_d
                except ZeroDivisionError:
                    pass
                else:
                    try:
                        expodatetime = datetime.datetime.strptime(exif.get_tag_string('Exif.Image.DateTime'), '%Y:%m:%d %H:%M:%S')
                    except TypeError:
                        pass
                    else:
                        print('{};{}'.format(expotime, expodatetime))

if __name__ == '__main__':
    expowalk('/home/steko/Immagini/')
